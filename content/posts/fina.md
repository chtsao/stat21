---
title: "Final Exam Announcement: Stat 2021"
date: 2021-06-14T14:55:24+08:00
draft: false
tags: []
categories: [adm]
---

<img src="https://www.mhmcintyre.us/wp-content/uploads/2014/08/Tale-of-two-cities.jpg" style="zoom:67%;" />

雙城記開卷經典名語(中英對照): 狄更斯 雙城記 
>它是最好的時代，也是最壞的時代；
>是智慧的時代，也是愚蠢的時代；
>是信仰的時代，也是懷疑的時代；
>是光明的季節，也是黑暗的季節；
>是充滿希望的春天，也是令人絕望的冬天；
>我們的前途擁有一切，我們的前途一無所有；
>我們正走向天堂，我們也走向地獄─
>總之，那個時代和現在是如此的相像，
>以至於它最喧鬧的一些專家，不論說好說壞，
>都堅持只能用最高級的形容詞來描述它。

>IT WAS the best of times, it was the worst of times,
>it was the age of wisdom, it was the age of foolishness,
>it was the epoch of belief, it was the epoch of incredulity,
>it was the season of Light, it was the season of Darkness,
>it was the spring of hope, it was the winter of despair,
>we had everything before us, we had nothing before us,
>we were all going direct to Heaven,
>we were all going direct the other way- in short,
>the period was so far like the present period,
>that some of its noisiest authorities insisted on its being received,
>for good or for evil, in the superlative degree of comparison only. 

[轉自 子昆部落格](https://ijk671.pixnet.net/blog/post/40335325)

## Finally, the Final
*  Date Start/Deadline: **0622（二）:1310/ 0623（三）:1159.**   
*  Submit your file to our **[Google Classroom](https://classroom.google.com/c/MzQzNTE1Mzk2MzU3) Classwork**. 
*  Take-Home Exam: 0622（二）:1310 Google Classroom 公佈考題。
*  規定：
  1. 可討論，可查課本，可參考筆記，可上網，可問人。(除了課本/筆記外的參考資料/人/網站在可知範圍盡量列出，寫在最後報告/答案中)
  2. 自己寫，自己知道，自己知道自己寫什麼，自己寫什麼自己知道
  3. 不可違反 2 所列之要求
* Format: **One** pdf or html file is preferred. Multiple pics are less welcome.  
* **遠距口試**: 為 釐清回答 以及確定 規定 之遵守度，我們會抽樣部份同學遠距口試。請於 0630 （三）左右查看學校 gmail 口試通知。
