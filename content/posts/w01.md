---
title: "Week 1. Overview"
date: 2021-03-03T15:50:27+08:00
tags: [random variable, model]
categories: [notes]
markup: "mmark"
---
<img src="https://tvline.com/wp-content/uploads/2020/12/the-queens-gambit-netflix-chess-ceiling.png?w=1024" style="zoom:67%;" />
### 統計學，這門課學什麼？
回顧一下，我們在基礎機率中學習了隨機變數 (random variable)
的基礎。用符號來傳達就是

$$\Large x \Rightarrow  X.$$

統計學這門課是在這個基礎上的進一步探究，可以說是

$$\Large X \Leftarrow x_1, ..., x_n $$

其中$$x_1, ..., x_n$$ 是由$$X$$來的一個樣本數(sample size)為$$n$$ 的抽樣(sample)。但這樣的設定不足以刻劃我們探究$$X$$使用方法的有效性或性質。因此我們真正討論的是

$$\Large X \Leftarrow X_1, ..., X_n$$

其中$$X_1, ..., X_n \sim X$$, say $$X$$ has pdf $$f_\theta$$. 

再次強調 大小寫差別 很重要, 這個符號上的差異反映也提醒我們 fixed 與 random 的不同。

### 思維架構
1. $$X \approx$$ 不確定, 風險, 隨機, 誤差 的一個模型 
2. $$X$$ 其實就是一個函數，如它的 pdf/pmf, cdf 等。一個一般的函數不容易描述，但透過它的形式與參數，我們可以將它類別化，使用類似的方法來處理，理解同一類的函數。例如 
$$X \sim $$ Bernoulli(p), $$p \in (0, 1).$$ 
這是民意調查，市占率等常用的一個模型。我們可透過了解 p 來了解$$X.$$

### 統計架構
Given this conceptual framework, we can **formulate** the problems of understanding a random variable $$X$$ as the related math/stat problems about its parameter. There are three main approaches/categories of the problems: Given a "good" sample of $$X$$, we would like to answer these questions

* Point Estimation: What is the value of the parameter?
* Interval Estimation: What is the possible range of the parameter?
* Hypothesis Testing: Which statement is more likely to be true, $$H_0$$ or $$H_1$$?

