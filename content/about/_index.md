---
title: "About"
date: 2021-02-18T04:56:45+08:00
draft: false
categories: [adm]
---
![Dice](https://www.simplilearn.com/ice9/free_resources_article_thumb/Data-Science-vs.-Big-Data-vs.jpg)

* Curator: Kno Tsao.  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1710 @ AE B101
* Google Classroom: [Kno.Stat 2021](https://classroom.google.com/c/MzQzNTE1Mzk2MzU3) (NEW! 2021.0518)
* Office Hours: Mon 12:00-12:50, Thr 15:10-16:00  @ SE A411
* TA Office Hours: 
  * 陳學莆: Tue 1610-1500. @A412
  * 劉映彤: Fri.  1310-1400. @A408
  * 蘇毅豪: Fri.  1240-1340. @A408
* Prerequisites: Calculus, Intro to Probability
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)